#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 05_marginal_tests_plot.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 05_marginal_tests_plot.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/05_marginal_tests_plot.R >log/05_marginal_tests_plot.log 2>&1
conda deactivate
