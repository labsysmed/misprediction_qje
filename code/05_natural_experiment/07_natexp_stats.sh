#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 07_natexp_stats.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 07_natexp_stats.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/07_natexp_stats.R >log/07_natexp_stats.log 2>&1
conda deactivate
