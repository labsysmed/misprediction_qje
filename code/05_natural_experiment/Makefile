# ------------------------------------------------------------------------------
# Makes all targets for 08_natural_experiments
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=10000]" make
# ------------------------------------------------------------------------------

# Config -----------------------------------------------------------------------
.ONESHELL:
include config.mk

# Recipes ----------------------------------------------------------------------
## all			: Produces all targets for shift natural experiment
.PHONY: all
all: output_tbls output_fgs

## leaveout_test_rates	: Calculates leave-one-out shift test rates for all encs
.PHONY: leaveout_test_rates
leaveout_test_rates: $(LEAVEOUT_TEST_RATES)
$(LEAVEOUT_TEST_RATES): 01_leaveout_shift_test_rates.sh \
scripts/01_leaveout_shift_test_rates.R $(COHORT) $(DIRS)
	bash $<

## iv_reg_tbls		: Builds LaTex tables with IV regression results
.PHONY: iv_reg_tbls
iv_reg_tbls: $(IV_REG_TBLS)
$(IV_REG_TBLS): 02_iv_regression_tbls.sh scripts/02_iv_regression_tbls.R \
$(LEAVEOUT_TEST_RATES)
	bash $<

## re_bins		: Bins shifts based on testing random effect
.PHONY: re_bins
re_bins: $(RE_BINS)

$(RE_MODELS): 03_shift_random_effects.sh scripts/03_shift_random_effects.R \
$(LEAVEOUT_TEST_RATES)
	bash $<

$(RE_BINS): 04_get_shift_coefs.sh scripts/04_get_shift_coefs.R \
$(RE_MODELS)
	bash $<

## marginal_tests		: Plots marginal test rate by shift bin
.PHONY: marginal_tests
marginal_tests: $(MARGINAL_TESTS)
$(MARGINAL_TESTS): 05_marginal_tests_plot.sh scripts/05_marginal_tests_plot.R \
$(RE_BINS)
	bash $<

## balance_plots		: Produces risk balance scatter
.PHONY: balance_plots
balance_plots: $(BALANCE_SCATTER)

$(BALANCE_SCATTER): 06_risk_balance_scatter.sh \
scripts/06_risk_balance_scatter.R $(LEAVEOUT_TEST_RATES)
	bash $<

## natexp_stats		: Reports various stats from natural experiment
.PHONY: natexp_stats
natexp_stats: $(NATEXP_STATS)
$(NATEXP_STATS): 07_natexp_stats.sh scripts/07_natexp_stats.R $(RE_BINS)
	bash $<

## excl_restriction_regs	: Checks exclusion restriction for test instrument
.PHONY: excl_restriction_regs
excl_restriction_regs: $(EXCL_RESTRICTION_REGS)

$(PREV_K_HRS): 08_tests_in_k_hours.sh scripts/08_tests_in_k_hours.R \
$(COHORT) $(DIRS)
	bash $<

$(EXCL_RESTRICTION_REGS): 09_check_excl_restriction.sh \
scripts/09_check_excl_restriction.R $(PREV_K_HRS)
	bash $<

## sim_testing		: Reports various stats re simulating dropping tests
.PHONY: sim_testing
sim_testing: $(SIM_TESTING)
$(SIM_TESTING): 10_simulating_testing.sh scripts/10_simulating_testing.R \
$(RE_BINS)
	bash $<

## output_tbls		: Copies tables used in paper to `output` directory
.PHONY: output_tbls
output_tbls: $(OUTPUT_TBLS)
$(OUTPUT_DIR)/tables/%.tex : %.tex
	cp $< $@

## output_fgs		: Copies tables used in paper to `output` directory
.PHONY: output_fgs
output_fgs: $(OUTPUT_FGS)
$(OUTPUT_DIR)/figures/%.png : %.png
	cp $< $@

# Config targets ---------------------------------------------------------------
## dirs 			: Builds all directories.
.PHONY : dirs
dirs : $(DIRS)
%/.f :
	mkdir -p $(dir $@)
	touch $@

## style			: Styles R scripts according to tidyverse guidelines
.PHONY: style
style:
	source ~/anaconda3/etc/profile.d/conda.sh
	conda activate stressr
	R --vanilla --silent -e "styler::style_dir(path = 'scripts', filetype = c('R', 'Rmd', 'Rprofile'))"

## clean			: Deletes all output.
.PHONY : clean
clean :
	rm -rf temp

.PHONY : help
help : Makefile
	@sed -n 's/^##//p' $<
