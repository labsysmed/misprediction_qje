#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 08_tests_in_k_hours.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=10000]" bash 08_tests_in_k_hours.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/08_tests_in_k_hours.R >log/08_tests_in_k_hours.log 2>&1
conda deactivate
