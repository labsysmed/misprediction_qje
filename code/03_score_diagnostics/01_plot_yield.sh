#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 01_plot_yield.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 01_plot_yield.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/01_plot_yield.R >log/01_plot_yield.log 2>&1
conda deactivate
