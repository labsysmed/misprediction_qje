#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 02_plot_adverse_outcomes.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 02_plot_adverse_outcomes.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/02_plot_adverse_outcomes.R >log/02_plot_adverse_outcomes.log 2>&1
conda deactivate
