#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 05_plot_stress_cath_calib.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 05_plot_stress_cath_calib.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/05_plot_stress_cath_calib.R >log/05_plot_stress_cath_calib.log 2>&1
conda deactivate
