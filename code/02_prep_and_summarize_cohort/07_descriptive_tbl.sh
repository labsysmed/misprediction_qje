#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 07_descriptive_tbl.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bash 07_descriptive_tbl.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/07_descriptive_tbl.R >log/07_descriptive_tbl.log 2>&1
conda deactivate
