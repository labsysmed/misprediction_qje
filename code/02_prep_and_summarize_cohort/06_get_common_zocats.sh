#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 06_get_common_zocats.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bash 06_get_common_zocats.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/06_get_common_zocats.R >log/06_get_common_zocats.log 2>&1
conda deactivate
