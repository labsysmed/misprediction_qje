#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 01_build_risk_factors.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=10000]" bash 01_build_risk_factors.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/01_build_risk_factors.R >log/01_build_risk_factors.log 2>&1
conda deactivate
