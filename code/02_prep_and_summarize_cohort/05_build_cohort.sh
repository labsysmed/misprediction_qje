#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 05_build_cohort.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bash 05_build_cohort.sh {SPLIT}
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr

if [ $1 == TRUE ]
then overnight=_overnight
else overnight=""
fi

Rscript --vanilla --verbose scripts/05_build_cohort.R --split $1 >log/05_build_cohort$overnight.log 2>&1
conda deactivate
