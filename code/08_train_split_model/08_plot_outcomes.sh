#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 08_plot_outcomes.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 08_plot_outcomes.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/08_plot_outcomes.R >log/08_plot_outcomes.log 2>&1
conda deactivate
