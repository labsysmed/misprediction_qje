#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 06_fit_ensemble.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 06_fit_ensemble.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/06_fit_ensemble.R >log/06_fit_ensemble.log 2>&1
conda deactivate
