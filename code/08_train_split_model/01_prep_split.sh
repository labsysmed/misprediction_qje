#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 01_prep_split.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 01_prep_split.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/01_prep_split.R >log/01_prep_split.log 2>&1
conda deactivate
