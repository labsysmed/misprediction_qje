#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 05_predict_subscores.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=25000]" bash 05_predict_subscores.sh [dataset]
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/05_predict_subscores.R --dataset $1 >log/05_predict_subscores_$1.log 2>&1
conda deactivate
