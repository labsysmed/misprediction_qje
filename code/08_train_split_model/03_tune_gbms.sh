#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 03_tune_gbms.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=25000]" bash 03_tune_gbms.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/03_tune_gbms.R >log/03_tune_gbms.log 2>&1
conda deactivate
