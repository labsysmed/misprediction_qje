VPATH = temp
DIRS = temp/.f log/.f temp/overnight/.f

COHORT = /data/zolab/stressed_ensemble/data/analysis/test_cohort.rds

COST_CURVES = temp/COST_CURVE_300_200.png temp/COST_CURVE_500_100.png

COST_STATS = log/02_cost_effectiveness_stats.log \
/data/zolab/stressed_ensemble/data/analysis/costeff_risk_cutoff.rds

COST_TBLS = temp/yield_costeff_tbl_cath_first.tex \
temp/yield_costeff_tbl_cath_all.tex \
temp/yield_costeff_tbl_stress_first.tex \
temp/yield_costeff_tbl.tex

MACE_V_YIELD_PLOT = temp/mace_vs_yield.png

OUTPUT_DIR = ../../../output

OUTPUT_TBL_FNAMES = yield_costeff_tbl_cath_first.tex \
yield_costeff_tbl_cath_all.tex yield_costeff_tbl_stress_first.tex \
yield_costeff_tbl.tex
OUTPUT_TBLS = $(foreach fn,$(OUTPUT_TBL_FNAMES),$(OUTPUT_DIR)/tables/$(fn))

OUTPUT_FG_FNAMES = COST_CURVE_300_200.png COST_CURVE_500_100.png \
mace_vs_yield.png
OUTPUT_FGS = $(foreach fn,$(OUTPUT_FG_FNAMES),$(OUTPUT_DIR)/figures/$(fn))
