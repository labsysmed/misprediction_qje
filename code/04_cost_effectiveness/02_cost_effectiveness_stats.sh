#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 02_cost_effectiveness_stats.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bash 02_cost_effectiveness_stats.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/02_cost_effectiveness_stats.R >log/02_cost_effectiveness_stats.log 2>&1
conda deactivate
