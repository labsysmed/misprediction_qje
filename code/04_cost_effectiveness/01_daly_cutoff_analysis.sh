#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 01_daly_cutoff_analysis.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bash 01_daly_cutoff_analysis.sh {overnight}
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr

if [ $1 == TRUE ]
then overnight=_overnight
else overnight=""
fi

Rscript --vanilla --verbose scripts/01_daly_cutoff_analysis.R --overnight $1 >log/01_daly_cutoff_analysis$overnight.log 2>&1
conda deactivate
