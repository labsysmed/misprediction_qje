# Cost Effectiveness
This directory takes as inputs the cost-effectiveness numbers calculated in `02_prep_and_summarize_cohort` and outputs several plots and analyses exploring the relationship between cost-effectiveness of testing and algorithm-predicted risk. More information on cost-effectiveness can be found in Appendix 2. These scripts construct Figure 1b, Figures A.1-2, and Tables A.3-5.
