#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 03_yield_costeff_table.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bash 03_yield_costeff_table.sh {overnight}
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/03_yield_costeff_table.R >log/03_yield_costeff_table.log 2>&1
conda deactivate
