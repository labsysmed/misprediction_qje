#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 01_get_representative_ccs.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 01_get_representative_ccs.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/01_get_representative_ccs.R >log/01_get_representative_ccs.log 2>&1
conda deactivate
