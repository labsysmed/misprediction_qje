VPATH = temp
DATA_DIR = /data/zolab/stressed_ensemble/data
DIRS = log/.f temp/.f

COHORT = $(DATA_DIR)/analysis/test_cohort.rds

REP_VARS = $(DATA_DIR)/analysis/representative_vars.rds
CC_FREQUENCY = temp/cc_frequency_df.rds
CC_FREQUENCY_TB = temp/symptom_representativeness.tex

REP_YHATS = $(DATA_DIR)/modeling/prediction/random/represent/scores_test_set.rds
CC_YHATS = $(DATA_DIR)/modeling/prediction/random/justcc/scores_test_set.rds

CC_SALIENCE_REG = temp/symptom_salience_regressions.tex

DEMOGRAPHIC_REG = temp/demographic_reg_tbl.tex

PHYS_EXPERIENCE_REG = temp/physician_volume_reg_tbl.tex

SUBCAT_STATS = log/05_subcat_risk_stat.log

SALIENCE_REG = temp/test_010_day__salience_regressions.tex \
temp/stent_or_cabg_010_day__salience_regressions.tex

SIMPLE_YHATS = /data/zolab/stressed_ensemble/data/analysis/simple_yhats.rds

BIAS_YHAT_PLOTS = temp/simplicity_bias_HR_rates.png \
temp/simplicity_bias_LR_rates.png temp/representative_bias_HR_rates.png \
temp/representative_bias_LR_rates.png

OUTPUT_DIR = ../../../output

OUTPUT_TBL_FNAMES = symptom_representativeness.tex \
symptom_salience_regressions.tex \
demographic_reg_tbl.tex test_010_day__salience_regressions.tex \
stent_or_cabg_010_day__salience_regressions.tex
OUTPUT_TBLS = $(foreach fn,$(OUTPUT_TBL_FNAMES),$(OUTPUT_DIR)/tables/$(fn))

OUTPUT_FG_FNAMES = simplicity_bias_HR_rates.png simplicity_bias_LR_rates.png \
representative_bias_HR_rates.png representative_bias_LR_rates.png
OUTPUT_FGS = $(foreach fn,$(OUTPUT_FG_FNAMES),$(OUTPUT_DIR)/figures/$(fn))
