#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 06_salience_regression_tbl.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 06_salience_regression_tbl.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/06_salience_regression_tbl.R >log/06_salience_regression_tbl.log 2>&1
conda deactivate
