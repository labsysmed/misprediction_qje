#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 03_demographic_risk_tbl.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 03_demographic_risk_tbl.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/03_demographic_risk_tbl.R >log/03_demographic_risk_tbl.log 2>&1
conda deactivate
