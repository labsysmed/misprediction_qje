#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 05_predict_ensemble_components.R in virtual environment stressr
# Author: Cassidy Shubatt <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=25000]" bash 05_predict_ensemble_components.sh {dataset} {overnight}
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr

if [ $2 == "overnight" ]
then split=_overnight
else split=""
fi

if [ $3 == "all" ]
then restriction=""
else restriction=_$3
fi

Rscript --vanilla --verbose scripts/05_predict_ensemble_components.R --dataset $1 --split $2 --restriction $3 >log/05_predict_ensemble_components_$1$split$restriction.log 2>&1
conda deactivate
