#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 01_create_ensembles.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 01_create_ensembles.sh {downsample} {split}
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr

if [ $1 == "overnight" ]
then split=_overnight
else split=""
fi

Rscript --vanilla --verbose scripts/01_create_ensembles.R --split $1 >log/01_create_ensembles$split.log 2>&1
conda deactivate
