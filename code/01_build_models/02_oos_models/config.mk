PRIMARY_MODELING=/data/zolab/stressed_ensemble/data/modeling
MODELING=$(PRIMARY_MODELING)/oos

TUNING=$(MODELING)/tuning/$(SPLIT)/$(RESTRICTION)
MODELS=$(MODELING)/models/$(SPLIT)/$(RESTRICTION)
SUBSCORES=$(MODELING)/subscores/$(SPLIT)/$(RESTRICTION)
PREDICTION=$(MODELING)/prediction/$(SPLIT)/$(RESTRICTION)
PRE_COHORTS=$(PRIMARY_MODELING)/cohorts/$(SPLIT)
COHORTS=$(MODELING)/cohorts/$(SPLIT)

MODELING_DIRS=$(MODELING) $(TUNING) $(MODELS) $(SUBSCORES) $(PREDICTION) $(COHORTS)

DOWNSAMPLES=ds_mace ds_test tested
COHORT_FNS=train_cohort.rds test_cohort.rds val_cohort.rds \
$(foreach ds,$(DOWNSAMPLES),train_cohort_$(ds).rds)
COHORT_FILES=$(foreach fn,$(COHORT_FNS),$(COHORTS)/$(fn))

DOWNSAMPLE_COHORTS=$(sort \
	$(filter $(COHORTS)/train_cohort_%.rds,$(COHORT_FILES)) \
)

OUTCOMES=stent_or_cabg_010_day__tested macetrop_030_pos__untested #test_010_day__all
