#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 03_predict_lasso.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q medium -R "rusage[mem=1000]" bash 03_predict_lasso.sh {outcome}
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/03_predict_lasso.R --target $1 >log/03_predict_lasso_$1.log 2>&1
conda deactivate
