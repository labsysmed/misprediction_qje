#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 02_fit_unpenalized_along_path.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q short -R "rusage[mem=100]" bash 02_fit_unpenalized_along_path.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/02_fit_unpenalized_along_path.R >log/02_fit_unpenalized_along_path.log 2>&1
conda deactivate
