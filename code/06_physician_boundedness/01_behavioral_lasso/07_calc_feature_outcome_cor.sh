#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 07_calc_feature_outcome_cor.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q medium -R "rusage[mem=1000]" bash 07_calc_feature_outcome_cor.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/07_calc_feature_outcome_cor.R >log/07_calc_feature_outcome_cor.log 2>&1
conda deactivate
