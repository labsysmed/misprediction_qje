#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 08_plot_feature_outcome_cor.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q medium -R "rusage[mem=1000]" bash 08_plot_feature_outcome_cor.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/08_plot_feature_outcome_cor.R >log/08_plot_feature_outcome_cor.log 2>&1
conda deactivate
