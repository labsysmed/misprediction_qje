DIRS = log/.f temp/.f temp/performance_plots/.f
DATA_DIR = /data/zolab/stressed_ensemble/data

COHORT = $(DATA_DIR)/analysis/test_cohort.rds
OUTCOMES = stent_or_cabg_010_day #test_010_day

LASSOS := $(foreach outcome,$(OUTCOMES), \
	temp/lasso__$(outcome).rds \
)
UNPENALIZED_MOD = temp/unpenalized_glm__stent_or_cabg_010_day.rds

LASSO_SCORES := $(foreach outcome,$(OUTCOMES), \
	temp/scores__lasso__$(outcome).rds \
)

UNPENALIZED_SCORES = temp/scores__glm__stent_or_cabg_010_day.rds

PERFORMANCE = temp/performance_obs.rds #temp/performance_boot.rds

PERFORMANCE_PLOT = temp/BEHAVIORAL_AUC.png temp/BEHAVIORAL_R2.png

FEATURE_CORR = temp/feature_outcome_cor_by_selection_r2.csv
FEATURE_CORR_PLOT = temp/LASSO-SCATTER_BY_TEST-010-DAY.png

LEFTOVER_SIGNAL = temp/leftover_signal_regs.tex
