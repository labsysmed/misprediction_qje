#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 03_summarize_performance.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 03_summarize_performance.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/03_summarize_performance.R >log/03_summarize_performance.log 2>&1
conda deactivate
