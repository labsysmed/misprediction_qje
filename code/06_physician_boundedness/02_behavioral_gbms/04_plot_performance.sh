#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 04_plot_performance.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bash 04_plot_performance.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/04_plot_performance.R >log/04_plot_performance.log 2>&1
conda deactivate
