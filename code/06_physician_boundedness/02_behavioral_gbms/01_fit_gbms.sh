#!/bin/bash

# ------------------------------------------------------------------------------
# Runs 01_fit_gbms.R in virtual environment stressr
# Author: cshubatt@gmail.com <cshubatt@gmail.com>
# To run: bsub -q big -R "rusage[mem=50000]" bash 01_fit_gbms.sh
# ------------------------------------------------------------------------------

source ~/anaconda3/etc/profile.d/conda.sh
conda activate stressr
Rscript --vanilla --verbose scripts/01_fit_gbms.R >log/01_fit_gbms.log 2>&1
conda deactivate
