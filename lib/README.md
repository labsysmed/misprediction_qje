# `lib` File Descriptions
Brief descriptions for the files contained within this directory.

- `aesthetics.R`: helper functions and variables for generating plots throughout the project
- `cc_labels.csv`: label mappings for chief complaint variables, used in creating a few plots
- `ccs_to_comorbidities.csv`: mapping from CCS categories to comorbidities, used in `code/02_prep_and_summarize_cohort/scripts/09_build_comorbidities.R`
- `comorbidity_weight.csv`: risk weights associated with comorbidity categories, used in `code/02_prep_and_summarize_cohort/scripts/09_build_comorbidities.R`
- `costeff_constants.yml`: costs and probabilities used to calculate cost-effectiveness in `code/02_prep_and_summarize_cohort/scripts/11_cost_effectiveness.R`
- `filepaths.yml`: stores various absolute filepaths within Partners servers (so that in general, scripts can reference filepaths more intuitively)
- `optima.ttf`: Optima font used in plots throughout the project
- `physician_info.csv`: data on individual physicians used for some physician behavior analyses
- `python_util.py`: Python script with functions for processes that are difficult or not possible in R, e.g. reading parquet files; used a few times with `reticulate`
- `README.md`: you're looking at it!
- `risk_factors.yml`: definitions of risk factors (e.g. atherosclerosis) in terms of diagnosis category variables, used in `code/02_prep_and_summarize_cohort/scripts/01_build_risk_factors.R`
- `stressr.yml`: conda environment in which all analyses were built to run; see [here](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html) for instructions on building an environment from a yaml file
- `summary_tbl_labels.csv`: labels for variables reported on in summary tables
- `surival_estimates.csv`: baseline gender survival estimates used to calculate life expectancy in `code/02_prep_and_summarize_cohort/scripts/10_calc_life_expectancy.R`
- `zocats_to_comorbidities.csv`: mapping from CCS categories to comorbidities, used in `code/02_prep_and_summarize_cohort/scripts/09_build_comorbidities.R`
